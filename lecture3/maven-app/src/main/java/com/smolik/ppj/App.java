package com.smolik.ppj;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //System.out.println( "Hello World!" );
    }

    public static boolean checkAnagram(String stringOne, String stringTwo){
        char[] first = stringOne.replaceAll("\\s","").toLowerCase().toCharArray(); 
        char[] second = stringTwo.replaceAll("\\s","").toLowerCase().toCharArray();
        if (first.length != second.length)
            return false;
        int[] counts = new int[26]; 
        for (int i = 0; i < first.length; i++){
            counts[first[i]-97]++;  
            counts[second[i]-97]--;   
        }
        for (int i = 0; i<26; i++)
            if (counts[i] != 0)
                return false;
        return true;
    }

}
