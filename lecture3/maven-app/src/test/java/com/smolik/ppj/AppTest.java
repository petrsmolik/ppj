package com.smolik.ppj;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void anagramCheckPositive()
    {
        assertTrue(App.checkAnagram("Clint Eastwood", "Old West Action"));
    }

    @Test
    public void anagramCheckPositive2()
    {
        assertTrue(App.checkAnagram("Tom Marvolo Riddle", "I Am Lord Voldemort"));
    }

    @Test
    public void anagramCheckPositive3()
    {
        assertTrue(App.checkAnagram("Jim Morrison", "Mr Mojo Risin"));
    }

    @Test
    public void anagramCheckPositive4()
    {
        assertTrue(App.checkAnagram("Doctor Who", "Torchwood"));
    }

    @Test
    public void anagramCheckNegative()
    {
        assertFalse(App.checkAnagram("reklama", "karemel"));
    }

    @Test
    public void anagramCheckNegative2()
    {
        assertFalse(App.checkAnagram("kotel", "lolet"));
    }

    @Test
    public void anagramCheckNegative3()
    {
        assertFalse(App.checkAnagram("Elvira Lemon", "Elena Rimlom"));
    }

    @Test
    public void anagramCheckNegative4()
    {
        assertFalse(App.checkAnagram("Carmilla", "Milarca"));
    }
}
